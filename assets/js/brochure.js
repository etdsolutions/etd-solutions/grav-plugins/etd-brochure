var brochure = window.popupBrochure = {

    $content: null,
    container: '',

    init: function(content, container) {

        this.$content = content;
        this.container = container.toString();

        this.bind();
    },

    bind: function () {
        $(document).ready(function() {
            $('#popup-brochure').removeClass('fade-out');
        });

        var self = this;
        $('body').append(this.$content);

        var email = $('#popup-brochure').find('input[name="data[email]"]').parent().parent().parent();
        email.css('display', 'block');

        var name = $('#popup-brochure').find('input[name="data[lastname]"]').parent().parent().parent();
        name.css('display', 'block');

        $(email).on('keyup', function(){
           if(this.value != '' || $(name).val() != ''){
               $('#popup-brochure .form-field').css('display', 'block');
           } else {
               $('#popup-brochure .form-field').css('display', 'none');
               $(email).parent().css('display', 'block');
               $(name).parent().css('display', 'block');
           }
        });

        $(name).on('keyup', function(){
            if(this.value != '' || $(email).val() != ''){
                $('#popup-brochure .form-field').css('display', 'block');
            } else {
                $('#popup-brochure .form-field').css('display', 'none');
                $(email).css('display', 'block');
                $(name).css('display', 'block');
            }
        });

        $('#dismiss-brochure').on('click', function(e) {
            e.preventDefault();
            document.cookie = 'brochure=dismiss;';
            $('#' + self.container).removeClass('d-sm-block');
            $('#btn-brochure').removeClass('d-none');
        });

        $('#btn-brochure').on('click', function(e){
            $('#' + self.container).addClass('d-sm-block');
            $('#btn-brochure').addClass('d-none');
        });

        $('#btn-brochure-second').on('click', function(e){
            $('#' + self.container).addClass('d-sm-block');
        });

        document.querySelector("#popup-brochure").addEventListener("submit", function(e){
            var t = new Date();
            t.setMilliseconds(t.getMilliseconds() + 360 * 864e+5);
            document.cookie = 'brochure=ok; expires=' + t.toUTCString();
        });
    }
};