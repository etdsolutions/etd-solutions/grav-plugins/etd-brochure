<?php

namespace Grav\Plugin;

use Grav\Common\Plugin;
use Grav\Common\Grav;
use RocketTheme\Toolbox\Event\Event;

/**
 * Class EtdBrochurePlugin
 * @package Grav\Plugin
 */
class EtdBrochurePlugin extends Plugin
{

    static $plugin_config;

    /**
     * @return array
     *
     * The getSubscribedEvents() gives the core a list of events
     *     that the plugin wants to listen to. The key of each
     *     array section is the event that the plugin listens to
     *     and the value (in the form of an array) contains the
     *     callable (or function) as well as the priority. The
     *     higher the number the higher the priority.
     */
    public static function getSubscribedEvents()
    {
        return [
            'onPluginsInitialized' => ['onPluginsInitialized', 0]
        ];
    }

    public static function getQuery()
    {
        return Grav::instance()['uri']->url;
    }

    /**
     * Initialize the plugin
     */
    public function onPluginsInitialized()
    {
        // Don't proceed if we are in the admin plugin
        if ($this->isAdmin()) {
            return;
        }

        $this->enable([
            'onTwigTemplatePaths' => ['onTwigTemplatePaths', 0],
            'onTwigSiteVariables' => ['onTwigSiteVariables', 0],
        ]);

        self::$plugin_config = $this->config();

        // Enable the main event we are interested in
        /*$this->enable([
            'onTwigExtensions' => ['onTwigExtensions', 0]
        ]);*/
    }


    /**
     * Add current directory to twig lookup paths.
     */
    public function onTwigTemplatePaths()
    {
        $this->grav['twig']->twig_paths[] = __DIR__ . '/templates';
    }

    /**
     * if enabled on this page, load the JS + CSS theme.
     */
    public function onTwigSiteVariables()
    {
        // Retrieve the cookie.
        $cookie = isset($_COOKIE['brochure']) ? (string) $_COOKIE['brochure'] : '';
        $config = self::$plugin_config;

        //var_dump($cookie !== "ok" && !isset($_SERVER['HTTP_USER_AGENT']) || stripos($_SERVER['HTTP_USER_AGENT'], 'Google Page Speed Insights') === false);

        // If the cookie has not been accepted yet.
        if($config['popup'] == 1){

                $this->grav['assets']->addJs('plugin://etd-brochure/assets/js/brochure.min.js');
                $this->grav['assets']->addCss('plugin://etd-brochure/assets/css/brochure.min.css', -999);

                $twig = $this->grav['twig'];
                $this->grav['assets']->addInlineJs($twig->twig->render('partials/popup-brochure.html.twig'));

        }

    }


}
